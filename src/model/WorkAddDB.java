package model;

public class WorkAddDB {
   public String StokName,Ham;
    public Integer StokKodu,Koli,WorkNo,Miktar;



    public WorkAddDB(int stokKodu, int workNo, int koli) {
        StokKodu = stokKodu;
        Koli = koli;
        WorkNo = workNo;
    }

    public void setStokName(String stokName) {
        StokName = stokName;
    }

    public void setHam(String ham) {
        Ham = ham;
    }

    public void setStokKodu(Integer stokKodu) {
        StokKodu = stokKodu;
    }

    public void setKoli(Integer koli) {
        Koli = koli;
    }

    public void setWorkNo(Integer workNo) {
        WorkNo = workNo;
    }

    public void setMiktar(Integer miktar) {
        Miktar = miktar;
    }

    public String getStokName() {
        return StokName;
    }

    public String getHam() {
        return Ham;
    }

    public Integer getStokKodu() {
        return StokKodu;
    }

    public Integer getKoli() {
        return Koli;
    }

    public Integer getWorkNo() {
        return WorkNo;
    }

    public Integer getMiktar() {
        return Miktar;
    }
}
