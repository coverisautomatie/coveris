package model;

public class CreateQRCodeRequest {
        private String workOrderNo;

    public String getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(String workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public CreateQRCodeRequest(String workOrderNo) {
        super();
        this.workOrderNo = workOrderNo;
    }
}
