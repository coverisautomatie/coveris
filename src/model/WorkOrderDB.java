package model;

public class WorkOrderDB {

    private String StokName,Date;
    private Integer InBox,Statu,StokCode;

    public WorkOrderDB(String StokName, String Date, int StokCode, int Statu , int InBox) {
        this.Date = Date;
        this.Statu = Statu;
        this.InBox = InBox;
        this.StokCode = StokCode;
        this.StokName = StokName;
    }

    public Integer getStokCode(){
        return StokCode;
    }

    public void setStokCode(Integer StokCode){
        this.StokCode = StokCode;
    }

    public String getDate(){
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getStokName(){
        return StokName;
    }

    public void setStokName(String stokName) {
        StokName = stokName;
    }

    public Integer getInBox(){
        return InBox;
    }

    public void setInBox(Integer Inbox) {
        this.InBox = InBox;
    }

    public Integer getStatu(){
        return Statu;
    }

    public void setStatus(Integer Statu) {
        this.Statu = Statu;
    }
}
