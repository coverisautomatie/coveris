package model;

import model.ConnectionDBMySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDb {
    private static Integer empId;
    private static String department;
    private static String empName;

    public static Integer getEmpId() {
        return empId;
    }

    public static void setEmpId(Integer empId) {
        LoginDb.empId = empId;
    }

    public static String getDepartment() {
        return department;
    }

    public static void setDepartment(String department) {
        LoginDb.department = department;
    }

    public static String getEmpName() {
        return empName;
    }

    public static void setEmpName(String empName) {
        LoginDb.empName = empName;
    }

    //Diğer sayfalarda kullanmak için veri tabanından dönen "personel numarası" ve "departman" bilgileri yukarıdaki
    //değişkenlere atanacak. Bu sınıftan instance oluşturup getter metotlarıyla ulaşabilirsiniz.

    public boolean checkInDb(Integer id, String department) {

        String sql = "SELECT * FROM personal Where id = ? and department = ?";

        try {
            Connection con = ConnectionDBMySQL.getConnect();

            PreparedStatement preparedStatement = con.prepareStatement(sql);

            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, department);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                setEmpId(resultSet.getInt("id"));
                setDepartment(resultSet.getString("department"));
                setEmpName(resultSet.getString("personal_name"));
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


    }


}