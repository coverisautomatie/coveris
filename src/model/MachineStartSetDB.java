package model;

import java.sql.Time;
import java.util.Date;
import java.util.Objects;

public class MachineStartSetDB {
    private String sebeb ;
    private String start,finish;

    public MachineStartSetDB(String sebeb, String start, String finish) {
        this.sebeb = sebeb;
        this.start = start;
        this.finish = finish;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStart() {
        return start;
    }

    public void setSebeb(String sebeb) {
        this.sebeb = sebeb;
    }

    public String getSebeb() {
        return sebeb;
    }
}
