/*
 *@author Ertuğrul Yakın
 */
package model;

import controller.Employee;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PersonelDb {
    ObservableList<Employee> e = FXCollections.observableArrayList();

    /*TODO: Metot bir obje array'i alıyor, objeler string değerler tutuyor, tarih dahil string olarak gönderiyorum bu metoda.
       Değerlere nasıl erişebileceğinize dair bir örnek bıraktım metodun içinde. Personel eklendikçe veya silindikçe
       bu metot çalışıyor, yapmanız gereken veri tabanında personel listesini güncellemek.*/
    public void updateEmployees(ObservableList<Employee> employeeList) {
        System.out.println("PersonelDb sınıfı updateEmployees() metoduna gelen güncellenmiş liste: ");
        int index = 0;
        for (Employee e: employeeList) {
            System.out.println("--------------\nindex: " + index);
            System.out.println("Ad:" + e.getName());
            System.out.println("Tc: " + e.getTc());
            System.out.println("Departman: " + e.getDepartment());
            System.out.println("Personel no: " + e.getId());
            System.out.println("İşe giriş tarihi: " + e.getEmploymentDate());
            index++;
        }
    }

    /*TODO: Personel bilgilerini veri tabanında nasıl tutacağızı söyledikten sonra içeriğini oluşturacağım metodun

     */
    public ObservableList<Employee> getEmployees() {
        //Verinin string olarak gelmesi gerekirse:
        e.add(new Employee("Ertuğrul Yakın", "12312312312", "Kalıplama", "989898",
                "22.01.2020"));
        // Her personel bir obje olarak gelirse
        /* e.add(new Employee(gelenVeri.name, gelenVeri.tc, ....));*/

        // Personel listesi bir obje array'i olarak gelirse
        // for(Object personel : gelenveriPersonelListesi) { .....}

        return e;
    }
}
