package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WorkOrderFollowDB {
    Integer WorkOrderNo,Plus,Uretilen,Kalan,Durum;


    public WorkOrderFollowDB() {
    }


    public WorkOrderFollowDB(Integer workOrderNo, Integer plus, Integer uretilen, Integer kalan, Integer durum) {
        WorkOrderNo = workOrderNo;
        Plus = plus;
        Uretilen = uretilen;
        Kalan = kalan;
        Durum = durum;
    }


    public void getTableData() {


    }

    public Integer getWorkOrderNo() {
        return WorkOrderNo;
    }

    public void setWorkOrderNo(Integer workOrderNo) {
        WorkOrderNo = workOrderNo;
    }

    public Integer getPlus() {
        return Plus;
    }

    public void setPlus(Integer plus) {
        Plus = plus;
    }

    public Integer getUretilen() {
        return Uretilen;
    }

    public void setUretilen(Integer uretilen) {
        Uretilen = uretilen;
    }

    public Integer getKalan() {
        return Kalan;
    }

    public void setKalan(Integer kalan) {
        Kalan = kalan;
    }

    public Integer getDurum() {
        return Durum;
    }

    public void setDurum(Integer durum) {
        Durum = durum;
    }
}
