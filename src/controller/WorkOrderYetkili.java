package controller;

import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.ConnectionDBMySQL;
import model.WorkOrderDB;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

public class WorkOrderYetkili implements Initializable {

    String stokAdiTable ;
    LocalDate tarihTable ;
    Integer stokKoduTable ;
    Integer koliIciTable ;
    Integer durumTable;


    @FXML private TableView<WorkOrderDB> tableView;
    @FXML private TableColumn<WorkOrderDB , String> StokNameCLM;
    @FXML private TableColumn<WorkOrderDB, LocalDate> DateCLM;
    @FXML private TableColumn<WorkOrderDB, Integer> StokCodeCLM;
    @FXML private TableColumn<WorkOrderDB, Integer> StatuCLM;
    @FXML private TableColumn<WorkOrderDB, Integer> InBoxCLM;
    @FXML private JFXTextField searchTXT;

    ObservableList<WorkOrderDB> data = FXCollections.observableArrayList();

    @FXML
    public void initialize (URL url , ResourceBundle rb) {

        try {

            // data base e bağlanma
             Connection con = ConnectionDBMySQL.getConnect();

           // istenilen verilerin sorgusunun yazıldığı yer
            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee");


               // sorugu sonrası gelen verilerin tek tek while döngüsüyle table a eklenmesi
            while (rs.next()){
                data.add(new WorkOrderDB(rs.getString("StokName"), rs.getString("Date"),
                        rs.getInt("StokCode"),rs.getInt("Statu"),rs.getInt("InBox")));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }


        // set up the columns in the table
        StokNameCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, String>("StokName"));
        DateCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, LocalDate>("Date"));
        StokCodeCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, Integer>("StokCode"));
        InBoxCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, Integer>("InBox"));
        StatuCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, Integer>("Statu"));

        // veri yükleme
     tableView.setItems(data);

    }

    @FXML
    public void refresh() {
        data.clear();

        try {
            Connection con = ConnectionDBMySQL.getConnect();

            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee");

            while (rs.next()){
                data.add(new WorkOrderDB(rs.getString("StokName"), rs.getString("Date"),
                        rs.getInt("StokCode"),rs.getInt("Statu"),rs.getInt("InBox")));
            }

        }
        catch (SQLException error) {
            System.out.println(error);
        }

        tableView.setItems(data);
    }

    @FXML
    public void search(ActionEvent e ) {
        String idSearch = searchTXT.getText();
        String sql = "select * from TableDenemee WHERE idTableDeneme = ?";
        try {
            data.clear();
            Connection con = ConnectionDBMySQL.getConnect();

            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1,idSearch);

            ResultSet rs = pr.executeQuery();


                while (rs.next()){
                    data.add(new WorkOrderDB(rs.getString("StokName"), rs.getString("Date"),
                            rs.getInt("StokCode"),rs.getInt("Statu"),rs.getInt("InBox")));


            };



        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        tableView.setItems(data);
    }

    @FXML
    public void selectSetItem(){
        this.stokAdiTable = tableView.getSelectionModel().getSelectedItem().getStokName();
        this.tarihTable = LocalDate.parse(tableView.getSelectionModel().getSelectedItem().getDate());
        this.stokKoduTable = tableView.getSelectionModel().getSelectedItem().getStokCode();
        this.koliIciTable = tableView.getSelectionModel().getSelectedItem().getInBox();
        this.durumTable = tableView.getSelectionModel().getSelectedItem().getStatu();
        System.out.println("dasdasda");
    }
    @FXML
    void editAction(ActionEvent event){


        System.out.println(stokAdiTable + "," + tarihTable + "," + stokKoduTable + "," + koliIciTable + "," + durumTable );

        if (stokAdiTable != null){
             Dialog<Pair<String, String>> dialog = new Dialog<>();
             dialog.setTitle("Düzenle");
             //dialog.setHeaderText("Look, a Custom Login Dialog");

            // Button özelliklerini güncelle.
             ButtonType loginButtonType = new ButtonType("Düzenle", ButtonBar.ButtonData.OK_DONE);
             dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

            // label ve textfield ların oluştrulduğu kısım.
             GridPane grid = new GridPane();
             grid.setHgap(10);
             grid.setVgap(10);
             grid.setPadding(new Insets(20, 150, 10, 10));

             TextField stokAdi = new TextField();
             stokAdi.setPromptText(this.stokAdiTable);

             TextField tarih = new TextField();
             tarih.setPromptText(String.valueOf(tarihTable));

             TextField stokKodu = new TextField();
             stokKodu.setPromptText(String.valueOf(stokKoduTable));

             TextField koliIci = new TextField();
             koliIci.setPromptText(String.valueOf(koliIciTable));

             TextField durum = new TextField();
             durum.setPromptText(String.valueOf(durumTable));

             grid.add(new Label("Stok Adı :"), 0, 0);
             grid.add(stokAdi, 1, 0);

             grid.add(new Label("Tarih :"), 0, 1);
             grid.add(tarih, 1, 1);

             grid.add(new Label("Stok Kodu :"), 0, 2);
             grid.add(stokKodu, 1, 2);

             grid.add(new Label("Koli İçi :"), 0, 3);
             grid.add(koliIci, 1, 3);

             grid.add(new Label("Durum :"), 0, 4);
             grid.add(durum, 1, 4);

            // Bir kullanıcı adının girilip girilmediğine bağlı olarak giriş düğmesini Etkinleştir / Devre Dışı Bırak.
             Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
             loginButton.setDisable(true);

            // Do some validation (using the Java 8 lambda syntax).
            stokAdi.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
             });

            tarih.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
            });

            stokKodu.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
            });
            koliIci.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
            });
            durum.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
            });


             dialog.getDialogPane().setContent(grid);


// Convert the result to a username-password-pair when the login button is clicked.
             dialog.setResultConverter(dialogButton -> {
                 if (dialogButton == loginButtonType) {
                     System.out.println("düzenle");
                 }
                 return null;
             });

            Optional<Pair<String, String>> result = dialog.showAndWait();

             //result.ifPresent(usernamePassword -> {
               //  System.out.println("Username=" + usernamePassword.getKey() + ", Password=" + usernamePassword.getValue());
             //});
        }else {

            Alert a = new Alert(Alert.AlertType.NONE);
            a.setAlertType(Alert.AlertType.WARNING);
            a.setTitle("boş değer");
            a.setContentText("boş değer");

            // show the dialog
            a.show();
            System.out.println("deneme");
        }


    }

    @FXML
    void deleteAction(ActionEvent e) {

        this.refresh();

    }



}
