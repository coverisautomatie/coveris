/*
 *@author Ertuğrul Yakın
 */
package controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class Navigator {
    public static Stage stage = new Stage();

    public void navigate( String destination) throws IOException {
        if(!destination.isEmpty()) {
            String fxmlFile = "../view/"+destination;

            FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlFile));
            Scene scene = new Scene(loader.load());
            stage.setScene(scene);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setMaximized(true);
            stage.setResizable(false);
            stage.getIcons().add(new Image("/resources/icon_app.png"));
            stage.show();
        }
        else {
            System.out.println("Hedef fxml gönderilmedi!");
        }
    }
}