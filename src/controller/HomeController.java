/*
 *@author Ertuğrul Yakın
 */
package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import javafx.beans.binding.ListBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import model.LoginDb;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public class HomeController implements Initializable {
    @FXML public HBox tabIsEmri;
    @FXML public HBox tabMakine;
    @FXML public HBox tabFire;
    @FXML public HBox tabGunSonu;
    @FXML public HBox tabPersonel;
    @FXML public JFXComboBox<String> macDropdown;
    @FXML public Button logoutBtn;
    @FXML public Label userName;
    @FXML public Label userDepartment;
    @FXML public ImageView poly;
    @FXML public JFXButton btnPer;
    @FXML public JFXButton btnGun;
    @FXML public JFXButton btnFire;
    @FXML public JFXButton btnMak;
    @FXML public JFXButton btnIs;
    @FXML public FlowPane tabContentContainer;
    @FXML public StackPane content;
    @FXML public BorderPane mainContainer;
    @FXML public VBox tabView;
    @FXML public JFXButton btnTopTabBarFirst;
    @FXML public JFXButton btnTopTabBarMid;
    @FXML public JFXButton btnTopTabBarLast;
    @FXML public HBox horizontalTabBarContainer;
    @FXML public VBox horizontalTabBtn1Container;
    @FXML public VBox horizontalTabBtn2Container;
    @FXML public VBox horizontalTabBtn3Container;

    private HBox[] verticalTabs;

    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private double screenHeight = screenSize.getHeight();
    private double screenWidth = screenSize.getWidth();
    private String selectedMachine = null; //getSelectedMac() metodu ile değerine erişebilirsiniz.
    private ObservableList<String> dropdownK = FXCollections.observableArrayList("K-001", "K-002", "K-003", "K-004",
            "K-005", "K-006", "K-007", "K-008", "K-009");
    private ObservableList<String> dropdownB = FXCollections.observableArrayList("B-001", "B-002", "B-003", "B-004"
            ,"B-005", "B-006", "B-007", "B-008", "B-009", "B-010", "B-011", "B-012", "B-013", "B-014", "B-015", "B-016", "B-017"
            ,"B-018", "B-019", "B-020", "B-021", "B-022");

    private ObservableList<String> dropdownF = FXCollections.observableArrayList("F-001");

    private FXMLLoader isEmriBaskiLoader = new FXMLLoader(getClass().getResource("/view/isEmriBaski.fxml"));
    private Parent isEmriBaski = isEmriBaskiLoader.load();

    private FXMLLoader isEmriKaliplamaLoader = new FXMLLoader(getClass().getResource("/view/isEmriKaliplama.fxml"));
    private Parent isEmriKaliplama = isEmriKaliplamaLoader.load();

    private FXMLLoader isEmriYetYonLoader = new FXMLLoader(getClass().getResource("/view/isEmriYetYon.fxml"));
    private Parent isEmriYetYon = isEmriYetYonLoader.load();

    private FXMLLoader isEmriEkleLoader = new FXMLLoader(getClass().getResource("/view/isEmriEkle.fxml"));
    private Parent isEmriEkle = isEmriEkleLoader.load();

    private FXMLLoader isEmriTakipLoader = new FXMLLoader(getClass().getResource("/view/isEmriTakip.fxml"));
    private Parent isEmriTakip = isEmriTakipLoader.load();

    private FXMLLoader urunBilgisiLoader = new FXMLLoader(getClass().getResource("/view/urunBilgisi.fxml"));
    private Parent urunBilgisi = urunBilgisiLoader.load();

    private FXMLLoader makineIstatistigiLoader = new FXMLLoader(getClass().getResource("/view/makineIstatistigi.fxml"));
    private Parent makineIstatistigi = makineIstatistigiLoader.load();

    private FXMLLoader makineCalismasiniAyarlaLoader = new FXMLLoader(getClass().getResource("/view/makineCalismasiniAyarla.fxml"));
    private Parent makineCalismasiniAyarla = makineCalismasiniAyarlaLoader.load();

    private FXMLLoader fireGirisiLoader = new FXMLLoader(getClass().getResource("/view/fireGirisi.fxml"));
    private Parent fireGirisi = fireGirisiLoader.load();

    private FXMLLoader fireDurumBilgisiLoader = new FXMLLoader(getClass().getResource("/view/fireDurumBilgisi.fxml"));
    private Parent fireDurumBilgisi = fireDurumBilgisiLoader.load();

    private FXMLLoader gunSonuLoader = new FXMLLoader(getClass().getResource("/view/gunSonu.fxml"));
    private Parent gunSonu = gunSonuLoader.load();

    private FXMLLoader personelLoader = new FXMLLoader(getClass().getResource("/view/personel.fxml"));
    private Parent personel = personelLoader.load();



    String loginDb;
    public HomeController() throws IOException {
        LoginDb loginD = new LoginDb();
        this.loginDb = loginD.getDepartment();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        personel.autosize();
        verticalTabs = new HBox[]{tabIsEmri, tabMakine, tabGunSonu, tabFire, tabPersonel};
        switch (loginDb) {
            case "Kalıplama": {
                setVerticalTabView(dropdownK, new String[]{"tabFire", "tabGunSonu", "tabPersonel"});
                selectedTabVertical(tabIsEmri);
                try {
                    setHorizontalTabView("Kalıplama", btnIs);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "Kalıplama-Yetkili" : {
                setVerticalTabView(dropdownK, new String[]{"tabPersonel"});
                selectedTabVertical(tabIsEmri);
                try {
                    setHorizontalTabView("Kalıplama-Yetkili", btnIs);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "Baskı": {
                setVerticalTabView(dropdownB, new String[]{"tabFire", "tabGunSonu", "tabPersonel"});
                selectedTabVertical(tabIsEmri);
                try {
                    setHorizontalTabView("Baskı", btnIs);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "Baskı-Yetkili": {
                setVerticalTabView(dropdownB, new String[]{"tabPersonel"});
                selectedTabVertical(tabIsEmri);
                try {
                    setHorizontalTabView("Baskı-Yetkili", btnIs);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "Fire": {
                setVerticalTabView(dropdownF, new String[]{"tabPersonel", "tabIsEmri", "tabMakine", "tabGunSonu"});
                selectedTabVertical(tabFire);
                try {
                    setHorizontalTabView("Fire", btnFire);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
            case "Yönetici": {
                ObservableList<String> s = combineObservableLists(dropdownK, dropdownB, dropdownF);
                macDropdown.setItems(s);
                macDropdown.setValue(s.get(1));

                try {
                    setHorizontalTabView("Yönetici", btnIs);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                selectedTabVertical(tabIsEmri);
                break;
            }
        }
        userName.setText("set");
        userDepartment.setText("birsey");
        listenerSetter();
    }

    private void setVerticalTabView(ObservableList<String> dropdown,  String[] remove) {
        for (String s : remove) {
            tabView.getChildren().remove(tabView.lookup("#" + s));
        }
        macDropdown.setItems(dropdown);
        macDropdown.setValue(dropdown.get(1));
    }

    private void listenerSetter() {
        JFXButton[] btnList = new JFXButton[]{btnIs, btnMak, btnFire, btnGun, btnPer};
        for (JFXButton btn: btnList) {
            btn.setOnAction(event -> {
                try {
                    setHorizontalTabView(loginDb, btn);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                HBox temp = (HBox) btn.getParent();
                selectedTabVertical(temp);
            });
        }
    }

    private void setHorizontalTabView( String user,  JFXButton btn) throws IOException {
        String id = btn.getId();
        switch (user) {
            case "Baskı": {
                if(id.equals("btnIs")) {
                    setHorizontalTabBarBtnText("İŞ EMİRLERİ LİSTESİ", "ÜRÜN BİLGİLERİ");
                    setHorizontalTabBarBtnAction(new Parent[]{isEmriBaski, urunBilgisi});
                    setTabContent(isEmriBaski);
                }
                else if (id.equals("btnMak")) {
                    setHorizontalTabBarBtnText("MAKİNE İSTATİSTİĞİ", "MAKİNE ÇALIŞMASINI AYARLA");
                    setHorizontalTabBarBtnAction(new Parent[]{makineIstatistigi, makineCalismasiniAyarla});
                    setTabContent(makineIstatistigi);
                }
                break;
            }
            case "Kalıplama": {
                if(id.equals("btnIs")) {
                    setHorizontalTabBarBtnText("İŞ EMİRLERİ LİSTESİ", "ÜRÜN BİLGİLERİ");
                    setHorizontalTabBarBtnAction(new Parent[]{isEmriKaliplama, urunBilgisi});
                    setTabContent(isEmriKaliplama);
                }
                else if (id.equals("btnMak")) {
                    setHorizontalTabBarBtnText("MAKİNE İSTATİSTİĞİ", "MAKİNE ÇALIŞMASINI AYARLA");
                    setHorizontalTabBarBtnAction(new Parent[]{makineIstatistigi, makineCalismasiniAyarla});
                    setTabContent(makineIstatistigi);
                }
                break;
            }
            case "Baskı-Yetkili": { }
            case "Kalıplama-Yetkili" : {
                switch (id) {
                    case "btnIs":
                        setHorizontalTabBarBtnText("İŞ EMİRLERİ LİSTESİ", "İŞ EMRİ EKLE", "İŞ EMRİ TAKİP");
                        setHorizontalTabBarBtnAction(new Parent[]{isEmriYetYon, isEmriEkle, isEmriTakip});
                        setTabContent(isEmriYetYon);
                        break;
                    case "btnMak":
                        setHorizontalTabBarBtnText("MAKİNE İSTATİSTİĞİ", "MAKİNE ÇALIŞMASINI AYARLA");
                        setHorizontalTabBarBtnAction(new Parent[]{makineIstatistigi, makineCalismasiniAyarla});
                        setTabContent(makineIstatistigi);
                        break;
                    case "btnFire":
                        setHorizontalTabBarBtnText("FİRE DURUM BİLGİSİ");
                        setHorizontalTabBarBtnAction(new Parent[]{fireDurumBilgisi});
                        setTabContent(fireDurumBilgisi);
                        break;
                    case "btnGun":
                        setHorizontalTabBarBtnText("GÜN SONU");
                        setHorizontalTabBarBtnAction(new Parent[]{gunSonu});
                        setTabContent(gunSonu);
                        break;
                }
                break;
            }
            case "Fire": {
                if (id.equals("btnFire")) {
                    setHorizontalTabBarBtnText("FİRE GİRİŞİ");
                    setHorizontalTabBarBtnAction(new Parent[]{fireGirisi});
                    setTabContent(fireGirisi);
                }
                break;
            }
            case "Yönetici": {
                switch (id) {
                    case "btnIs":
                        setHorizontalTabBarBtnText("İŞ EMİRLERİ LİSTESİ", "İŞ EMRİ EKLE", "İŞ EMRİ TAKİP");
                        setHorizontalTabBarBtnAction(new Parent[]{isEmriYetYon, isEmriEkle, isEmriTakip});
                        setTabContent(isEmriYetYon);
                        break;
                    case "btnMak":
                        setHorizontalTabBarBtnText("MAKİNE İSTATİSTİĞİ", "MAKİNE ÇALIŞMASINI AYARLA");
                        setHorizontalTabBarBtnAction(new Parent[]{makineIstatistigi, makineCalismasiniAyarla});
                        setTabContent(makineIstatistigi);
                        break;
                    case "btnFire":
                        setHorizontalTabBarBtnText("FİRE DURUM BİLGİSİ");
                        setHorizontalTabBarBtnAction(new Parent[]{fireDurumBilgisi});
                        setTabContent(fireDurumBilgisi);
                        break;
                    case "btnGun":
                        setHorizontalTabBarBtnText("GÜN SONU");
                        setHorizontalTabBarBtnAction(new Parent[]{gunSonu});
                        setTabContent(gunSonu);
                        break;
                    case "btnPer":
                        setHorizontalTabBarBtnText("PERSONEL");
                        setHorizontalTabBarBtnAction(new Parent[]{personel});
                        setTabContent(personel);
                        break;
                }
                break;
            }
        }
    }

    private void setHorizontalTabBarBtnText(String firstBtnText, String midBtnText, String lastBtnText) {
        toggleButtonVisibility(3);
        btnTopTabBarFirst.setText(firstBtnText);
        btnTopTabBarMid.setText(midBtnText);
        btnTopTabBarLast.setText(lastBtnText);
    }

    private void setHorizontalTabBarBtnText(String firstBtnText, String midBtnText) {
        toggleButtonVisibility(2);
        btnTopTabBarFirst.setText(firstBtnText);
        btnTopTabBarMid.setText(midBtnText);
    }

    private void setHorizontalTabBarBtnText(String firstBtnText) {
        toggleButtonVisibility(1);
        btnTopTabBarFirst.setText(firstBtnText);
    }

    private void toggleButtonVisibility(int numOfVisibleButtons) {
        String[] btnArray = {".tabBarTopBtnFirst", ".tabBarTopBtnMid", ".tabBarTopBtnLast"};
        for (int i=0; i<3; i++) {
            horizontalTabBarContainer.lookup(btnArray[i]).setVisible(false);
            horizontalTabBarContainer.lookup(btnArray[i]).setDisable(true);
        }
        for (int i=0; i<numOfVisibleButtons; i++) {
            horizontalTabBarContainer.lookup(btnArray[i]).setVisible(true);
            horizontalTabBarContainer.lookup(btnArray[i]).setDisable(false);
        }
    }

    private void setHorizontalTabBarBtnAction( Parent[] parent) {
        JFXButton[] temp = {btnTopTabBarFirst, btnTopTabBarMid, btnTopTabBarLast};
        for(int i=0; i<parent.length; i++) {
            int finalI = i;
            temp[i].setOnAction(actionEvent -> {
                try {
                    setTabContent(parent[finalI]);
                   /* HBox parentContainer = (HBox) temp[finalI].getParent();
                   selectedTabHorizontal((VBox) parentContainer.lookup(".polyDown"));*/ //TODO: yatay tab menüsü okları
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void setTabContent( Parent parent) throws IOException {
        content.getChildren().clear();
        parent.prefWidth(screenHeight/1.26);
        parent.prefHeight(screenWidth/1.16);
        content.getChildren().add(parent);
    }


    @SafeVarargs
    private static <T> ObservableList<T> combineObservableLists(ObservableList<T>... sources) {
        return new ListBinding<>() {
            {
                bind(sources);
            }

            @Override
            protected ObservableList<T> computeValue() {
                return FXCollections.concat(sources);
            }
        };
    }

    private void selectedTabVertical(HBox selected) {
        ImageView backPolygonContainer = new ImageView();
        backPolygonContainer.getStyleClass().add("backPolygonContainer");
        Image backPolygon = new Image("/resources/main/poly_back.png");
        backPolygonContainer.setImage(backPolygon);

        for(int i=0 ; i<5 ; i++) {
            verticalTabs[i].getChildren().remove(verticalTabs[i].lookup(".backPolygonContainer"));
        }
        selected.getChildren().add(backPolygonContainer);
    }

    //TODO: yatay tab menüsü okları
  /*  private void selectedTabHorizontal(VBox selected){
        ImageView upPolygonContainer = new ImageView();
        upPolygonContainer.setStyle("-fx-alignment: POS");
        upPolygonContainer.getStyleClass().add("upPolygonContainer");
        Image upPolygon = new Image("/resources/main/poly_up.png");
        upPolygonContainer.setImage(upPolygon);

        horizontalTabBtn1Container.getChildren().remove(horizontalTabBtn1Container.lookup(".upPolygonContainer"));
        horizontalTabBtn2Container.getChildren().remove(horizontalTabBtn2Container.lookup(".upPolygonContainer"));
        horizontalTabBtn3Container.getChildren().remove(horizontalTabBtn3Container.lookup(".upPolygonContainer"));
        selected.getChildren().add(upPolygonContainer);
    }
*/
    @FXML
    public void onLogoutPressed() {
        System.exit(0);
    }

    public String getSelectedMachine() {
        return selectedMachine;
    }
}
