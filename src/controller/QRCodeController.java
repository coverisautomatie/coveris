package controller;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import model.CreateQRCodeRequest;

import java.nio.file.FileSystems;
import java.nio.file.Path;

public class QRCodeController {

    private String QRCODE_PATH = "src/resources/QRCode";

    public String writeQRCode(CreateQRCodeRequest createQRCodeRequest) throws Exception {

        String qrcode = QRCODE_PATH + createQRCodeRequest.getWorkOrderNo() + "-QRCODE.png";

        QRCodeWriter writer = new QRCodeWriter();

        BitMatrix byteMatrix = writer.encode(
                String.valueOf(createQRCodeRequest.getWorkOrderNo()), BarcodeFormat.QR_CODE,350,350
        );

        Path path = FileSystems.getDefault().getPath(qrcode);
        MatrixToImageWriter.writeToPath(byteMatrix,"PNG",path);

        return "Create QRCode Succesfull...";
    }

    public static void main(String[] args) {

    }
}
