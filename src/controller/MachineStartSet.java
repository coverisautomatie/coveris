package controller;

import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalField;
import java.util.*;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import model.ConnectionDBMySQL;
import model.MachineStartSetDB;

public class MachineStartSet {


    String sebebText,startTime,finishTime,firstStartTime;
    Integer counter = 0;
    Integer min = 0,second = 0,hours = 0;
    Integer Pmin = 0,Psecond = 0,Phours = 0;
    Integer Smin = 0,Ssecond = 0,Shours = 0;

    @FXML
    private ResourceBundle resources;
    @FXML
    private URL location;
    @FXML
    private JFXTextField sebebLBL;
    @FXML
    private Label workLBL;
    @FXML
    private Label pauseLBL;
    @FXML
    private Label stopLBL;
    @FXML
    private TableColumn<MachineStartSetDB, String> sebebTBL;
    @FXML
    private TableColumn<MachineStartSetDB, String> startTBL;
    @FXML
    private TableColumn<MachineStartSetDB, String> finishTBL;
    @FXML
    private TableView<MachineStartSetDB> tableview;

    ObservableList<MachineStartSetDB> data = FXCollections.observableArrayList();

    Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
       second++;
        if (second == 60) {
            min++;
            second = 0;
        }
        if (min == 60) {
            hours++;
            min = 0;
        }
        workLBL.setText(hours +":"+min+":"+second);

    }), new KeyFrame(Duration.seconds(1)));

    Timeline Pclock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
        Psecond++;
        if (Psecond == 60) {
            Pmin++;
            Psecond = 0;
        }
        if (Pmin == 60) {
            Phours++;
            Pmin = 0;
        }

        pauseLBL.setText(Phours+":"+Pmin+":"+Psecond);
    }), new KeyFrame(Duration.seconds(1)));

    Timeline Sclock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
        Ssecond++;
        if (Ssecond == 60) {
            Smin++;
            Ssecond = 0;
        }
        if (Smin == 60) {
            Shours++;
            Smin = 0;
        }

        stopLBL.setText(Shours+":"+Smin+":"+Ssecond);

    }), new KeyFrame(Duration.seconds(1)));




    @FXML
    void startBTN(ActionEvent event) {
        Sclock.stop();
        Pclock.stop();
        this.Pmin = 0;
        this.Psecond = 0;
        this.Phours = 0;
        LocalTime time = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        if (counter == 0 ){

            clock.setCycleCount(Animation.INDEFINITE);
            clock.play();
            counter = 1;
        }
        if (this.startTime != null && this.sebebText != null) {

            finishTime = time.format(formatter);

            try {
                Connection con = ConnectionDBMySQL.getConnect();

                String sql = "INSERT INTO MachineSetStart (id, sebebi, start, finish) VALUES (NULL, ?, ?, ?)";


                PreparedStatement pr = con.prepareStatement(sql);

                pr.setString(1,this.sebebText);
                pr.setString(2,this.startTime);
                pr.setString(3,this.finishTime);
                pr.execute();



                con.close();
                getData();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            startTime = null;
            sebebTBL = null;


        }
    }

    @FXML
    void stopBTN(ActionEvent event) {

        clock.stop();

        Sclock.setCycleCount(Animation.INDEFINITE);
        Sclock.play();
        Pclock.setCycleCount(Animation.INDEFINITE);
        Pclock.play();

        this.counter = 0;
        LocalTime time = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        this.startTime = time.format(formatter);
        this.sebebText = sebebLBL.getText();
    }


    @FXML
    void initialize() {

    }

// makine durup başladıktan sonra tabloya veriler ekleniyor
    public void getData() {

        data.clear();

        try {

            Connection con = ConnectionDBMySQL.getConnect();
            //gerekli verinin sorgusu
            String sql = "Select * from MachineSetStart";

            ResultSet rs = con.createStatement().executeQuery(sql);


            // sebebi , start , finish veri tabanında ki tablo veri başlıkları
            while (rs.next()){
                data.add(new MachineStartSetDB(rs.getString("sebebi"), rs.getString("start"),
                        rs.getString("finish")));
            };

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sebebTBL.setCellValueFactory(new PropertyValueFactory<MachineStartSetDB, String>("sebeb"));
        startTBL.setCellValueFactory(new PropertyValueFactory<MachineStartSetDB, String>("start"));
        finishTBL.setCellValueFactory(new PropertyValueFactory<MachineStartSetDB, String>("finish"));

        tableview.setItems(data);

    }
}
