package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import model.PersonelDb;

public class PersonelController {
    @FXML
    private JFXDatePicker employmentDateField;
    @FXML
    private JFXTextField idField;
    @FXML
    private JFXComboBox<String> departmentField;
    @FXML
    private JFXTextField tcField;
    @FXML
    private JFXTextField nameField;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnDel;
    @FXML
    private TableView<Employee> personelTable;
    @FXML
    private TableColumn<Employee, String> colName;
    @FXML
    private TableColumn<Employee, String> colDepartment;
    @FXML
    private TableColumn<Employee, String> colTc;
    @FXML
    private TableColumn<Employee, String> colId;
    @FXML
    private TableColumn<Employee, String> colEmploymentDate;

    private PersonelDb p = new PersonelDb();
    private ObservableList<Employee> employees = FXCollections.observableArrayList();
    private ObservableList<String> departmentFieldOpts = FXCollections.observableArrayList("Kalıplama", "Kalıplama-Yetkili",
            "Baskı", "Baskı-Yetkili", "Fire", "Yönetici");

    public PersonelController() {
        //TODO: veri tabanı sonrası
        /*employees = p.getEmployees();*/
    }

    @FXML
    public void initialize() {
        departmentField.setItems(departmentFieldOpts);
        personelTable.setPlaceholder(new Label("Personel bilgisi bulunamadı"));
        colName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        colDepartment.setCellValueFactory(cellData -> cellData.getValue().departmentProperty());
        colTc.setCellValueFactory(cellData -> cellData.getValue().tcProperty());
        colId.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        colEmploymentDate.setCellValueFactory(cellData -> cellData.getValue().employmentDateProperty());
        employmentDateField.getEditor().setDisable(true);

        btnAdd.setOnAction(actionEvent -> {
            if(checkFields()) {
                 Employee e = new Employee(nameField.getText(), tcField.getText(), departmentField.getValue(),
                        idField.getText(), (employmentDateField.getValue().toString()).equals("") ?
                        employmentDateField.getEditor().getText() : employmentDateField.getValue().toString());
                 if(employees.isEmpty()){
                     employees.add(e);
                     personelTable.setItems(employees);
                     clearFields();
                     return;
                 }
                 else {
                     for (Employee value : employees) {
                         if (value.getId().equals(e.getId())) {
                             ButtonType type = new ButtonType("Tamam", ButtonBar.ButtonData.OK_DONE);
                             Alert alert = new Alert(Alert.AlertType.NONE, "Personel numarası listede zaten mevcut!",
                                     type);
                             alert.setTitle("Hata!");
                             alert.setHeaderText("Personel Eklenmedi:");
                             Stage alertStage = (Stage) alert.getDialogPane().getScene().getWindow();
                             alertStage.getIcons().add(new Image("/resources/personel/icon_error.png"));
                             alert.showAndWait();
                             idField.clear();
                             idField.setStyle("-fx-border-color: red");
                             return;
                         }
                     }
                 }
                 employees.add(e);
                 personelTable.setItems(employees);
                 clearFields();
            }
        });

        btnDel.setOnAction(actionEvent -> {
            ObservableList<Employee> selected = personelTable.getSelectionModel().getSelectedItems();
            Employee e =(Employee) selected.toArray()[0];
            for (int i=0; i<employees.size(); i++) {
                if(employees.get(i).getId().equals(e.getId())) {
                    employees.remove(i);
                    return;
                }
            }
            personelTable.getItems().removeAll(personelTable.getSelectionModel().getSelectedItems());
        });
        employees.addListener((ListChangeListener<Employee>) change -> {
            System.out.println("**********************\nPERSONEL LİSTESİ DEĞİŞTİ!");
            p = new PersonelDb();
            p.updateEmployees(employees);
        });
    }

    private void clearFields() {
        nameField.clear();
        tcField.clear();
        departmentField.setValue(null);
        idField.clear();
        employmentDateField.setValue(null);
        employmentDateField.getEditor().clear();
    }

    private boolean checkFields() {
        JFXTextField[] fieldsArray = {nameField, tcField, idField};
        employmentDateField.setStyle("-fx-border-color: lightgray");
        departmentField.setStyle("-fx-border-color: lightgray");
        for(JFXTextField f :fieldsArray ) {
            f.setStyle("-fx-border-color: lightgray");
        }
        if (nameField.getText().isEmpty()) {
            nameField.setStyle("-fx-border-color: red;");
            return false;
        }
        else if (tcField.getText().isEmpty()) {
            tcField.setStyle("-fx-border-color: red;");
            return false;
        }
        else if(departmentField.getValue() == null) {
            departmentField.setStyle("-fx-border-color: red;");
            return false;
        }
        else if(idField.getText().isEmpty()) {
            idField.setStyle("-fx-border-color: red;");
            return false;
        }
        else if(employmentDateField.getValue() == null) {
            employmentDateField.setStyle("-fx-border-color: red");
            return false;
        }
        return true;
    }
}
