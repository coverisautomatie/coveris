/*
 *@author Ertuğrul Yakın
 */
package controller;

import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.LoginDb;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class LoginController {
    @FXML public VBox blurRect;
    @FXML public JFXComboBox<String> department;
    @FXML public JFXTextField empId;
    @FXML public StackPane btnStackPane;
    @FXML public VBox pane;
    JFXPopup warn = new JFXPopup();
    Label popupLabel = new Label();

    ObservableList<String> dropdownOpt = FXCollections.observableArrayList("Kalıplama", "Kalıplama-Yetkili",
            "Baskı", "Baskı-Yetkili", "Fire", "Yönetici");
    Navigator navigator = new Navigator();


    String dep;
    @FXML
    public void initialize() {

        System.out.println("loginnCont");
        department.setItems(dropdownOpt);
        popupLabel.setAlignment(Pos.CENTER);
        popupLabel.setStyle("-fx-font-size: 1.3em; -fx-padding: 15; -fx-text-fill: white;" +
                " -fx-background-color: rgb(235, 77, 77);");
    }

    @FXML
    void loginPress() throws IOException {
        if(department.getValue()==null) {
            department.setStyle("-fx-prompt-text-fill: red;");
            popupCreator("Departman alanı boş olamaz!");
        }
        else if(empId.getText().isEmpty()) {
            popupCreator("Personel Numarası alanı boş olamaz!");
        }
        else {
            /*navigator.navigate("spinner.fxml");*/ //TODO: veri tabanı bağlanıtısından sonra spinner düzenlenip açılacak.
            LoginDb loginDb = new LoginDb();
            dep = department.getValue();
            AtomicBoolean loginStatus = new AtomicBoolean(false); //?

            try {
                loginStatus.set(loginDb.checkInDb(Integer.valueOf(empId.getText()), department.getValue()));
            } catch (Exception e) {
                loginStatus.set(false);
                System.out.println("Db hatası:" + e);
            }

            if (loginStatus.get()) {
                Stage currentStage = (Stage) pane.getScene().getWindow();
                currentStage.close();
                LoginDb log = new LoginDb();
                System.out.println("log.array.get(0)");
                loginSuccess();
            }
            else {
                loginFail();
            }
        }
    }

    public void loginSuccess() throws IOException {
        System.out.println("Logged in");
        navigator.navigate("home.fxml");

    }

    private void loginFail() {
        popupCreator("Kullanıcı bulunamadı, bilgileri kontrol edin!");
    }

    private void popupCreator(String message) {
        popupLabel.setText(message);
        warn.setPopupContent(popupLabel);
        warn.show(btnStackPane);
    }
}
