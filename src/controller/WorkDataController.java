package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.ConnectionDBMySQL;
import model.CreateQRCodeRequest;
import model.WorkOrderDB;

public class WorkDataController {

    int id;

    public WorkDataController(int id) {
        this.id = id;
    }

    public WorkDataController() {
        this.id = 3;
    }


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Label workNoLBL;

    @FXML
    private Label stokNameLBL;

    @FXML
    private Label stokKodLBL;

    @FXML
    private Label inBoxLBL;

    @FXML
    private Label dateLBL;

    @FXML
    private JFXTextField hammaddeTXT;

    @FXML
    private JFXButton etiketBTN;

    @FXML
    private Label toplaLBL;

    @FXML
    private Label uretilenLBL;

    @FXML
    private Label kalanLBL;

    @FXML
    void initialize() {


        getData(id);
        workData(id);
    }

    @FXML
    void EtiketBTN(ActionEvent event) {
        // QR Code

        QRCodeController code = new QRCodeController();
        try {
            System.out.println(code.writeQRCode(new CreateQRCodeRequest("emir hadi olm veri yolla")));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    // id değerine iş emri id girilecek
    public void getData(int id) {
        try {

            Connection con = ConnectionDBMySQL.getConnect();
            // sorgu
            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee");


            while (rs.next()){
                //idtable gerekli verinin table ismi
                if (rs.getInt("idTableDeneme") == id ){
                    workNoLBL.setText(rs.getString("StokName"));
                    stokKodLBL.setText(String.valueOf(rs.getInt("StokCode")));
                    stokNameLBL.setText(rs.getString("StokName"));
                    inBoxLBL.setText(String.valueOf(rs.getInt("InBox")));
                    dateLBL.setText(rs.getString("Date"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void workData(int id){
        try {

            Connection con = ConnectionDBMySQL.getConnect();
            // sorgu
            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee");


            while (rs.next()){
                //idtable gerekli verinin table ismi
                if (rs.getInt("idTableDeneme") == id ){
                    toplaLBL.setText(rs.getString("StokName"));
                    uretilenLBL.setText(rs.getString("Date"));
                    kalanLBL.setText(String.valueOf(rs.getInt("InBox")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
