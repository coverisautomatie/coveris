package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.ConnectionDBMySQL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MachineStatisticsController {

    int id;

    public MachineStatisticsController(int id) {
        this.id = id;
    }

    public MachineStatisticsController() {
        this.id = 3;
    }

    @FXML
    private Label grcKoliLBL;

    @FXML
    private Label vardiyaKoliLBL;

    @FXML
    private Label gnlkKoliLBL;

    @FXML
    private Label klnKoliLBL;

    @FXML
    private Label prsKoliLBL;

    @FXML
    private Label gerecekAdetLBL;

    @FXML
    private Label vardiyaAdetLBL;

    @FXML
    private Label gnlkAdetLBL;

    @FXML
    private Label klnAdetLBL;

    @FXML
    private Label prsAdetLBL;

    @FXML
    private Label standartTaksLBL;

    @FXML
    private Label standarAdetLBL;

    @FXML
    private Label grckTaksLBL;

    @FXML
    private Label grckAdetLBL;

    @FXML
    void initialize() {
        getBottomData(id);
        getTopData(id);
    }
    // sorgular ve veri isimleri tamamen deneme amaçlı veri tabanını bağlayıp gerekli sorgudan sonra veri isimlerini gerekli yere
    // yazmanız durumunda çalışıcaktır
    public void getTopData(int id){
        try {
            Connection con = ConnectionDBMySQL.getConnect();

            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee ");

            while (rs.next()){
                if (rs.getInt("idTableDeneme") == this.id){

                    grcKoliLBL.setText(rs.getString("StokName"));
                    gerecekAdetLBL.setText(rs.getString("StokName"));
                    vardiyaAdetLBL.setText(rs.getString("StokName"));
                    vardiyaKoliLBL.setText(rs.getString("StokName"));
                    gnlkAdetLBL.setText(rs.getString("StokName"));
                    gnlkKoliLBL.setText(rs.getString("StokName"));
                    klnAdetLBL.setText(rs.getString("StokName"));
                    klnKoliLBL.setText(rs.getString("StokName"));
                    prsAdetLBL.setText(rs.getString("StokName"));
                    prsKoliLBL.setText(rs.getString("StokName"));

                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // sorgular ve veri isimleri tamamen deneme amaçlı veri tabanını bağlayıp gerekli sorgudan sonra veri isimlerini gerekli yere
    // yazmanız durumunda çalışıcaktır
    public  void getBottomData(int id){
        try {
            Connection con = ConnectionDBMySQL.getConnect();

            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee ");

            while (rs.next()) {
                if (rs.getInt("idTableDeneme") == this.id){
                    standartTaksLBL.setText(rs.getString("StokName"));
                    standarAdetLBL.setText(rs.getString("Date"));
                    grckTaksLBL.setText(String.valueOf(rs.getInt("StokCode")));
                    grckAdetLBL.setText(String.valueOf(rs.getInt("InBox")));
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
