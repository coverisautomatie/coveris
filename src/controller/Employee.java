package controller;

import javafx.beans.property.*;

public class Employee {
    public StringProperty name;
    public StringProperty tc;
    public StringProperty department;
    public StringProperty id;
    public StringProperty employmentDate;

    public Employee() {

    }

    public Employee(String name, String tc, String department, String id, String employmentDate) {
        this.name = new SimpleStringProperty(name);
        this.tc = new SimpleStringProperty(tc);
        this.department = new SimpleStringProperty(department);
        this.id = new SimpleStringProperty(id);
        this.employmentDate = new SimpleStringProperty(employmentDate);
    }

    public String getName() {
        return name.get();
    }

    public String getDepartment() {
        return department.get();
    }

    public String getTc() {
        return tc.get();
    }

    public String getId() {
        return id.get();
    }

    public String getEmploymentDate() {
        return  employmentDate.get();
    }

    //TODO: Düzenleme eklenirse kullanılacak setter metotları
   /* public void setName(String name) {
        this.name.set(name);
    }

    public void setDepartment(String department) {
        this.department.set(department);
    }

    public void setTc(String tc) {
        this.tc.set(tc);
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setEmploymentDate(String employmentDate) {
        this.employmentDate.set(employmentDate);
    }*/

    public StringProperty nameProperty() {
        return name;
    }

    public StringProperty departmentProperty() {
        return department;
    }

    public StringProperty tcProperty() {
        return tc;
    }

    public StringProperty idProperty() {
        return id;
    }

    public StringProperty employmentDateProperty(){
        return employmentDate;
    }
}
