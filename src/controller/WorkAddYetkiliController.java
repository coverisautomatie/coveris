package controller;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.ConnectionDBMySQL;
import model.MachineStartSetDB;
import model.WorkAddDB;
import model.WorkOrderDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WorkAddYetkiliController {

    @FXML
    private JFXTextField StokNameTXT;

    @FXML
    private JFXTextField StokKodTXT;

    @FXML
    private JFXTextField WorkNoTXT;

    @FXML
    private JFXTextField InBoxTXT;

    @FXML
    private JFXTextField HammTXT;

    @FXML
    private JFXTextField MiktarTXT;

    @FXML
    private TableView<WorkAddDB> tableview;

    @FXML
    private TableColumn<WorkAddDB, Integer> StokKodTBL;

    @FXML
    private TableColumn<WorkAddDB, Integer>  WorkNoTBL;

    @FXML
    private TableColumn<WorkAddDB, Integer>  InBoxTBL;


    @FXML
    void initialize() {


    }


    @FXML
      void add(ActionEvent event) {
        try {
            Connection con = ConnectionDBMySQL.getConnect();

            String sql = "INSERT INTO WorkAdd (id, StokAdi, StokKodu, WorkNo, Koli, Ham, Miktar)" +
                    " VALUES (NULL, ?, ?, ?, ?, ?, ?);";

            PreparedStatement pr = con.prepareStatement(sql);

            pr.setString(1,StokNameTXT.getText());
            pr.setInt(2, Integer.parseInt(StokKodTXT.getText()));
            pr.setInt(3, Integer.parseInt(WorkNoTXT.getText()));
            pr.setInt(4, Integer.parseInt(InBoxTXT.getText()));
            pr.setString(5,HammTXT.getText());
            pr.setInt(6, Integer.parseInt(MiktarTXT.getText()));

            pr.execute();


            con.close();



        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    ObservableList<WorkAddDB> data = FXCollections.observableArrayList();
    @FXML
    void refreshBTN(ActionEvent event) {
        data.clear();

        try {
            Connection con = ConnectionDBMySQL.getConnect();

            ResultSet rs = con.createStatement().executeQuery("SELECT StokKodu,WorkNo,Koli FROM WorkAdd");

            while (rs.next()){
               // data.add(new MachineStartSetDB(rs.getString("sebebi"), rs.getString("start"),
                 //       rs.getString("finish")));
                data.add(new WorkAddDB(rs.getInt("stokKodu"),rs.getInt("WorkNo") ,rs.getInt("Koli")));
            };

        } catch (SQLException e) {
            e.printStackTrace();
        }

        StokKodTBL.setCellValueFactory(new PropertyValueFactory<WorkAddDB, Integer>("stokKodu"));
        WorkNoTBL.setCellValueFactory(new PropertyValueFactory<WorkAddDB, Integer>("workNo"));
        InBoxTBL.setCellValueFactory(new PropertyValueFactory<WorkAddDB, Integer>("koli"));

tableview.setItems(data);

    }
}
