package controller;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.ConnectionDBMySQL;
import model.WorkOrderDB;
import model.WorkOrderFollowDB;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class WorkOrderFollowController implements Initializable {
    @FXML
    private TableView<WorkOrderFollowDB> tableView;
    @FXML
    private TableColumn<WorkOrderFollowDB, Integer> WorkOrderCLM;
    @FXML
    private TableColumn<WorkOrderFollowDB, Integer> PlusCLM;
    @FXML
    private TableColumn<WorkOrderFollowDB, Integer> UretilenCLM;
    @FXML
    private TableColumn<WorkOrderFollowDB, Integer> KalanCLM;
    @FXML
    private TableColumn<WorkOrderFollowDB, Integer> StatuCLM;

    @FXML private JFXTextField searchTXT;

    ObservableList<WorkOrderFollowDB> data = FXCollections.observableArrayList();

    @FXML
    public void initialize (URL url , ResourceBundle rb) {
        try {
            Connection con = ConnectionDBMySQL.getConnect();
            ResultSet rs = con.createStatement().executeQuery("select * from Follow");

            while (rs.next()) {
                data.add(new WorkOrderFollowDB(rs.getInt("workNo"),rs.getInt("toplam"),rs.getInt("durum"),
                        rs.getInt("uretilen"),rs.getInt("kalan")));
            }

            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        WorkOrderCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderFollowDB, Integer>("WorkOrderNo"));
        PlusCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderFollowDB, Integer>("plus"));
        UretilenCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderFollowDB, Integer>("uretilen"));
        KalanCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderFollowDB, Integer>("kalan"));
        StatuCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderFollowDB, Integer>("durum"));


       tableView.setItems(data);



    }


    @FXML
    public void refresh() {

        data.clear();

        try {
            Connection con = ConnectionDBMySQL.getConnect();
            ResultSet rs = con.createStatement().executeQuery("select * from Follow");

            while (rs.next()) {
                data.add(new WorkOrderFollowDB(rs.getInt("workNo"),rs.getInt("toplam"),rs.getInt("durum"),
                        rs.getInt("uretilen"),rs.getInt("kalan")));
            }

            con.close();

        }
        catch (SQLException error) {
            System.out.println(error);
        }

        tableView.setItems(data);

    }


    @FXML
    public void search(ActionEvent e ) {

        String idSearch = searchTXT.getText();
        String sql = "select * from Follow WHERE id = ?";
        try {
            data.clear();
            Connection con = ConnectionDBMySQL.getConnect();

            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1,idSearch);

            ResultSet rs = pr.executeQuery();


            while (rs.next()) {
                data.add(new WorkOrderFollowDB(rs.getInt("workNo"),rs.getInt("toplam"),rs.getInt("durum"),
                        rs.getInt("uretilen"),rs.getInt("kalan")));
            }

            con.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        tableView.setItems(data);

    }




}
