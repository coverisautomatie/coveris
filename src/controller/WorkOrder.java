package controller;

import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.ConnectionDBMySQL;
import model.WorkOrderDB;
import javafx.event.ActionEvent;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class WorkOrder implements Initializable {
    @FXML private TableView<WorkOrderDB> tableView;
    @FXML private TableColumn<WorkOrderDB , String> StokNameCLM;
    @FXML private TableColumn<WorkOrderDB, LocalDate> DateCLM;
    @FXML private TableColumn<WorkOrderDB, Integer> StokCodeCLM;
    @FXML private TableColumn<WorkOrderDB, Integer> StatuCLM;
    @FXML private TableColumn<WorkOrderDB, Integer> InBoxCLM;
    @FXML private JFXTextField searchTXT;

    ObservableList<WorkOrderDB> data = FXCollections.observableArrayList();

    @FXML
    public void initialize (URL url , ResourceBundle rb) {

        try {

            // data base e bağlanma
             Connection con = ConnectionDBMySQL.getConnect();

           // istenilen verilerin sorgusunun yazıldığı yer
            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee");


               // sorugu sonrası gelen verilerin tek tek while döngüsüyle table a eklenmesi
            while (rs.next()){
                data.add(new WorkOrderDB(rs.getString("StokName"), rs.getString("Date"),
                        rs.getInt("StokCode"),rs.getInt("Statu"),rs.getInt("InBox")));
            }

        } catch (SQLException e) {
            System.out.println(e);
        }


        // set up the columns in the table
        StokNameCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, String>("StokName"));
        DateCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, LocalDate>("Date"));
        StokCodeCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, Integer>("StokCode"));
        InBoxCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, Integer>("InBox"));
        StatuCLM.setCellValueFactory(new PropertyValueFactory<WorkOrderDB, Integer>("Statu"));

        // veri yükleme
     tableView.setItems(data);

    }

    @FXML
    void selectAction(ActionEvent event) {
        String stokAdi = tableView.getSelectionModel().getSelectedItem().getStokName();
        LocalDate tarih = LocalDate.parse(tableView.getSelectionModel().getSelectedItem().getDate());
        Integer stokKodu = tableView.getSelectionModel().getSelectedItem().getStokCode();
        Integer koliIci = tableView.getSelectionModel().getSelectedItem().getInBox();
        Integer durum = tableView.getSelectionModel().getSelectedItem().getStatu();

        System.out.println(stokAdi + "," + tarih + "," + stokKodu + "," + koliIci + "," + durum );

    }

    @FXML
    public void refresh(ActionEvent event) {
        data.clear();

        try {
            Connection con = ConnectionDBMySQL.getConnect();

            ResultSet rs = con.createStatement().executeQuery("select * from TableDenemee");

            while (rs.next()){
                data.add(new WorkOrderDB(rs.getString("StokName"), rs.getString("Date"),
                        rs.getInt("StokCode"),rs.getInt("Statu"),rs.getInt("InBox")));
            }

        }
        catch (SQLException error) {
            System.out.println(error);
        }

        tableView.setItems(data);
    }

    @FXML
    public void search(ActionEvent e ) {
        String idSearch = searchTXT.getText();
        String sql = "select * from TableDenemee WHERE idTableDeneme = ?";
        try {
            data.clear();
            Connection con = ConnectionDBMySQL.getConnect();

            PreparedStatement pr = con.prepareStatement(sql);
            pr.setString(1,idSearch);

            ResultSet rs = pr.executeQuery();


                while (rs.next()){
                    data.add(new WorkOrderDB(rs.getString("StokName"), rs.getString("Date"),
                            rs.getInt("StokCode"),rs.getInt("Statu"),rs.getInt("InBox")));


            };



        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        tableView.setItems(data);
    }




}
