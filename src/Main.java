/*
 *@author Ertuğrul Yakın
 */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    double loginScHeight = screenSize.getHeight()*0.9;
    double loginScWidth = loginScHeight*1.56;


    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader =new FXMLLoader(getClass().getResource("view/login.fxml"));
        Parent root = loader.load();

        primaryStage.setTitle("COVERIS");
        primaryStage.setWidth(loginScWidth);
        primaryStage.setHeight(loginScHeight);

        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("/resources/icon_app.png"));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
